#ifndef DATASET_HPP
#define	DATASET_HPP

#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <utility>
////////////////////////////////////////////////////////////////////////////////////////
//Input
//-vetor de entradas com uma amostra de cada variavel de entrada
////////////////////////////////////////////////////////////////////////////////////////

class DataSet
{
	public:
		DataSet(std::vector<double> input,std::vector<double> desired_output);
		std::map<double,double> getInputSamples();
	private:
		std::map<double,double> inputSamples;//input as key and desired output as value.

};
#endif //	DATASET_HPP
