#include <iostream>
#include <sstream>
#include <ctime>
#include "layer.hpp"

class Mlp
{
	private:
		std::vector<Layer*> layers;//all the layers of the MLP, begin and End are always the first hidden and output layer,respectively.
		double expected_error;
		double learning_rate;
		double momentum;
		long int max_iterations;
		int num_hidden_layers;

	public:
		Mlp(double expecError,double learningRate, double momentumRate, double maxIterations,std::vector<Layer*>);
		Mlp(std::ifstream confFile);
		void train();
		void confusionMatrix();
		void confNNArch(std::ifstream file);
		void outputNNData();
};

class NNConf
{

};

//TODO::
// -> Criar destrutor de cada classe
// -> Finalizar classe de Configuracao da rede
// -> Criar funcao de receber um arquivo de configuracao das layers e rede
