#ifndef LAYER_HPP
#define LAYER_HPP
#include "neuron.hpp"

////////////////////////////////////////////////////////////////////////////////////////
//Layer:
// -Neurons
// -Inputs
// -Outputs
////////////////////////////////////////////////////////////////////////////////////////

class Layer
{
	public:
		Layer(const int countNeurons,const short int actFunc);
		void distributeDataSet(DataSet inputs);
		void feedForward();
		std::vector<double> getOutputs();
	private:
		std::vector<Neuron> Neurons;
		std::vector<double> inputs;
};

#endif //LAYER_HPP
