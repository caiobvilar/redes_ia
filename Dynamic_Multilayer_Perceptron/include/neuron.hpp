#ifndef NEURON_HPP
#define NEURON_HPP
#include "dataset.hpp"

enum ActivFunction: short int{ SIGMD, TANHIP, LIN, ATAN};

////////////////////////////////////////////////////////////////////////////////////////
//Neuronios:
//-Inputs
//-Weights
//-Activation Function
//-Local Gradient
////////////////////////////////////////////////////////////////////////////////////////

class Neuron
{
	public:
		Neuron(const short int actFunc,const short int prop,std::vector<double> inputsVec);
		Neuron(const short int prop, std::vector<double> inputsVec);
		void updateInputs(std::vector<double> newInputs);
		double getOutput();
	private:
		short int func;
		std::vector<double>weights;
		double output;
		std::vector<double> inputs;
		double rand01();
		double LocalGradient;
		void findInducedLocalField();//returns the sum of the multiplication of the inputs by the weights
		void updateOutput();
		void populateWeights();
		double ATanActiv(double v, double proport);
		double TanHiperActiv(double v, double proport);
		double LinActiv(double v, double proport);
		double sigmAtiv(double v);
};

#endif //NEURON_HPP
