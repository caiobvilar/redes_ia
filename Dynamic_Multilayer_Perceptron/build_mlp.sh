g++ src/dataset.cpp -c -Wall -Werror -ggdb -std=c++11 -fPIC
g++ src/neuron.cpp -c -Wall -Werror -ggdb -std=c++11 -fPIC
g++ src/layer.cpp -c -Wall -Werror -ggdb -std=c++11 -fPIC
#g++ src/multilayerPercep.cpp -c -Wall -Werror -ggdb -std=c++11 -fPIC
#g++ src/mlpTraining.cpp src/multilayerPercep.o src/layer.o src/neuron.o src/dataset.o -o mlpTrain -Wall -Werror -ggdb -std=c++11
