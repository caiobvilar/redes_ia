#include <map>
#include <algorithm>
#include <random>
#include <ctime>
#include <iostream>
#include <ctime>
int myrandom (int i) { return std::rand()%i;}

int main()
{

	srand(time(NULL));
	std::vector<double>keys;
	std::vector<double>values;
	std::vector<double>::iterator keysITR;
	std::vector<double>::iterator valuesITR;

	keys.push_back(-2.43);
	keys.push_back(0.57);
	keys.push_back(-0.22);
	keys.push_back(2.36);
	keys.push_back(0.82);
	keys.push_back(-1.1);
	keys.push_back(0.24);
	keys.push_back(2.61);
	keys.push_back(0.00);
	keys.push_back(1.54);
	keys.push_back(-0.8);
	keys.push_back(-1.06);

	values.push_back(0.01);
	values.push_back(0.02);
	values.push_back(0.03);
	values.push_back(0.04);
	values.push_back(0.05);
	values.push_back(0.06);
	values.push_back(0.07);
	values.push_back(0.08);
	values.push_back(0.09);
	values.push_back(0.10);
	values.push_back(0.11);
	values.push_back(0.12);
	std::map<double,double> mapper;
	std::map<double,double>::iterator mapperITR;
	for(keysITR = keys.begin(),valuesITR = values.begin();keysITR != keys.end(),valuesITR != values.end();keysITR++,valuesITR++)
	{
		mapper.insert(std::pair<double,double>((*keysITR),(*valuesITR)));
	}
	for(mapperITR = mapper.begin();mapperITR != mapper.end();mapperITR++)
	{
		std::cout << "KEY: " << (*mapperITR).first << " | VALUE: " << (*mapperITR).second << std::endl;
	}
	std::random_shuffle(keys.begin(),keys.end(),myrandom);
	std::cout << "AFTER RANDOMIZATION..."<< std::endl;
	for(keysITR = keys.begin();keysITR != keys.end();keysITR++)
	{
		mapperITR = mapper.find((*keysITR));
		std::cout << "KEY: " << (*keysITR) << " | VALUE: " << (*mapperITR).second << std::endl;
	}
}
