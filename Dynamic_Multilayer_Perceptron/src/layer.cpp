#include "../include/layer.hpp"

Layer::Layer(const int countNeurons,const short int actFunc)
{
	
}


////////////////////////////////////////////////////////////////////////////
//@parameters																															//
//-Dataset-inputs: inputs to the neurons																	//
//@return: void																														//
//#Description: copies a dataset of inputs to each of the neurons in the 	//
//layers Neuron vector, for them to calculate their local fields					// 
////////////////////////////////////////////////////////////////////////////
void Layer::distributeDataSet(DataSet inputs)
{
	std::map<double,double>::iterator datasetITR;
	std::vector<Neuron>::iterator neuronITR;
	this->inputs.clear();
	for(datasetITR = inputs.getInputSamples().begin();datasetITR != inputs.getInputSamples().end();datasetITR++)
	{
		this->inputs.push_back((*datasetITR).first);
	}
	for(neuronITR = this->Neurons.begin();neuronITR != this->Neurons.end();neuronITR++)
	{
		(*neuronITR).updateInputs(this->inputs);
	}
}

void Layer::feedForward()
{

}


std::vector<double> Layer::getOutputs()
{
	std::vector<Neuron>::iterator neuronITR;
	std::vector<double> outputs;
	for(neuronITR = this->Neurons.begin();neuronITR != this->Neurons.end();neuronITR++)
	{
		outputs.push_back((*neuronITR).getOutput());
	}
	return outputs;
}
