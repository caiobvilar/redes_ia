#include "../include/dataset.hpp"
DataSet::DataSet(std::vector<double>input, std::vector<double>desired_output)
{
	std::vector<double>::iterator inputITR;
	std::vector<double>::iterator desOutputITR;
	for(inputITR = input.begin(),desOutputITR = desired_output.begin();inputITR != input.end(),desOutputITR != desired_output.end();inputITR++,desOutputITR++)
	{
		this->inputSamples.insert(std::pair<double,double>((*inputITR),(*desOutputITR)));
	}
}

std::map<double,double> DataSet::getInputSamples()
{
	return this->inputSamples;
}

