#include "multilayerPercep.hpp"
#define ETA	0.1
#define ALPHA	0.001
#define MAX_ERROR 0.0001
#define MAX_ITERATIONS 100000

int main(int argc, char *argv[])
{
	//single hidden layer MLP, contains input,hidden and output layer.
	//
	std::vector<Layer*> layersVec;
	Layer *LInput = new Layer();//first hidden layer
	Layer *LOutput = new Layer();//output layer
	layersVec.push_back(LInput);
	layersVec.push_back(LOutput);
	MLP* mlp1 = new Mlp(MAX_ERROR,ETA,ALPHA,MAX_ITERATIONS,layersVec);
	mlp1->train();


	return 0;
}
