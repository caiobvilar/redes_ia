#include "../include/neuron.hpp"

Neuron::Neuron(const short int actFunc,const short int prop,std::vector<double> inputsVec)
{
	switch(actFunc)
	{
		case SIGMD:
			this->func = SIGMD;
			break;
		case ATAN:
			this->func = ATAN;
			break;
		case TANHIP:
			this->func = TANHIP;
			break;
		case LIN:
			this->func = LIN;
			break;
		default:
			this->func = SIGMD;
			break;
	}
	this->inputs = inputsVec;
	//Initialize weights
	for(int i=0;i < int(this->inputs.size());i++)
	{
		this->weights.push_back(rand01());
	}
}

Neuron::Neuron(const short int prop, std::vector<double> inputsVec)
{
	this->inputs = inputsVec;
	//Initialize weights
	for(int i=0;i < int(this->inputs.size());i++)
	{
		this->weights.push_back(rand01());
	}
}

void Neuron::findInducedLocalField()
{
	std::vector<double>::iterator inputITR;
	std::vector<double>::iterator weightITR;

	for(weightITR = this->weights.begin(),
			inputITR = this->inputs.begin();
			inputITR != this->inputs.end(),
			weightITR != this->weights.end();
			inputITR++,
			weightITR++)
	{
		this->output += ((*inputITR) * (*weightITR));
	}
}

void Neuron::updateInputs(std::vector<double> newInputs)
{
	this->inputs.clear();
	this->inputs = newInputs;
}

void Neuron::updateOutput()
{
	this->findInducedLocalField();
}
double Neuron::getOutput()
{
	return this->output;
}

double Neuron::sigmAtiv(double v)//funcao sigmoide
{
	double y;
	y = (1/(1+exp((v)*(-1)))); //retorna o valor V apos ser aplicado a funcao de ativacao
	return y;
}

double Neuron::LinActiv(double v, double proport)
{
	double ret = v*proport;
	return ret;
}

double Neuron::TanHiperActiv(double v, double proport)
{
	return tanh(v*proport);
}

double Neuron::ATanActiv(double v, double proport)
{
	return atan(v*proport);
}

double Neuron::rand01()// funcao que gera valores aleatorios entre 0 e 1
{
	return (double)(rand())/(double)(RAND_MAX);
}
