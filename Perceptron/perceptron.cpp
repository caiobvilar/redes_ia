#include <iostream>
#include <ctime>
#include <cmath>
#include <vector>
#include <algorithm>

#define ETA 0.01 //1*e-4
#define ERRESP 0.0001
double rand01()// funcao que gera valores aleatorios entre 0 e 1
{
	return (double)(rand())/(double)(RAND_MAX);
}

double funcAtiv(double v)//funcao sigmoide
{
	double y = (1/(1+exp((v)*(-1)))); //retorna o valor V apos ser aplicado a funcao de ativacao 
	return y;
}

class entradas
{
	public:
		entradas(double biasin,double x1in,double x2in,double esperadoin)
		{
			bias = biasin;
			x2 = x2in;
			x1 = x1in;
			esperado = esperadoin;
		}
		double bias;
		double x2;
		double x1;
		double esperado;
};

struct Pesos
{
	double w0;
	double w1;
	double w2;
};

int main(int argc, char *argv[])
{
	srand(unsigned(time(NULL)));
	//	const float ERRESP = 0.0001;
	std::vector<entradas> entradasTrein;
	std::vector<entradas> entradasValid;
	std::vector<entradas>::iterator itrinp;
	double v = 0;
	double error = 1;
	double y = 0;
	int epochs = 0;
	Pesos pesosAT;
	Pesos pesosPrev;
	pesosAT.w0 = rand01();
	pesosAT.w1 = rand01();
	pesosAT.w2 = rand01();
	pesosPrev = pesosAT;
	std::cout << "RANDOM Weights | W0: " << pesosAT.w0 << " | W1: " << pesosAT.w1 << " | W2: " << pesosAT.w2 << std::endl;
	//preenchendo as entradas de treinamento 
	//
	entradasTrein.push_back(entradas(-1, 0, 0.45,1));	//classe 1
	entradasTrein.push_back(entradas(-1,0.3,0.5,1));	//classe 1
	entradasTrein.push_back(entradas(-1,0.5,0.6,1));	//classe 1
	entradasTrein.push_back(entradas(-1,0.9,0.8,1));	//classe 1

	entradasTrein.push_back(entradas(-1,0.2,0.7,2));	//classe 2
	entradasTrein.push_back(entradas(-1,0,0.6,2));	  //classe 2
	entradasTrein.push_back(entradas(-1,0.4,0.8,2));	//classe 2
	entradasTrein.push_back(entradas(-1,0.5,0.8,2));	//classe 2
	entradasTrein.push_back(entradas(-1,0.8,1,2));	  //classe 2
	entradasTrein.push_back(entradas(-1,0,1.4,2));	  //classe 2
	entradasTrein.push_back(entradas(-1,0.2,1.3,2));	//classe 2

	//preenchendo as entradas de validacao

	entradasValid.push_back(entradas(-1, 1, 0.9,1));	//classe 1
	entradasValid.push_back(entradas(-1,0.4,0.2,1));	//classe 1
	entradasValid.push_back(entradas(-1,0.7,0.5,1));	//classe 1

	entradasValid.push_back(entradas(-1,0.4,1.2,2));	//classe 2
	entradasValid.push_back(entradas(-1,0.6,1.1,2));	  //classe 2
	entradasValid.push_back(entradas(-1,0.7,1,2));	//classe 2
	entradasValid.push_back(entradas(-1,0.4,1,2));	//classe 2
	entradasValid.push_back(entradas(-1,0.5,1,2));	  //classe 2
	entradasValid.push_back(entradas(-1,1,0.8,2));	  //classe 2
	entradasValid.push_back(entradas(-1,0.2,1.2,2));	  //classe 2


	while(error > ERRESP)//embaralha o vetor de entradas e realiza o treinamento novamente ate que o erro seja menor que o erro esperado
	{	
		std::random_shuffle(entradasTrein.begin(),entradasTrein.end());//embarlha as entradas
		itrinp = entradasTrein.begin();
		std::cout << "Entrada 1: "<< "Bias: " << (*itrinp).bias << " | X1: " << (*itrinp).x1 << " | X2: " << (*itrinp).x2 << std::endl;
		for(itrinp = entradasTrein.begin(); itrinp != entradasTrein.end();itrinp++)//faz o feedfoward e LMS para cada entrada
		{
			v = (pesosAT.w0 * (*itrinp).bias) + (pesosAT.w1 * (*itrinp).x1) + ((*itrinp).x2 * pesosAT.w2);//calcula o somatorio da multiplicacao das entradas
			//pelos pesos respectivos.
			std::cout << "V: " << v << std::endl;
			y = funcAtiv(v); //passa o resultado da soma das multiplicacoes das entradas pelos pesos respectivos pela
			//funcao de ativacao

			error = (*itrinp).esperado - y; //calcula o erro para o valor esperado desse cenario

			std::cout << "Error: " << error << std::endl;

			std::cout << "AT | W0: " << pesosAT.w0 << " | W1: "<< pesosAT.w1 << " | W2: " << pesosAT.w2 << std::endl;

			pesosAT.w0 = pesosPrev.w0 + (ETA * error * ((*itrinp).bias)); //calcula novo valor de w0
			pesosAT.w1 = pesosPrev.w1 + (ETA * error * ((*itrinp).x1)); //calcula novo valor de w1
			pesosAT.w2 = pesosPrev.w2 + (ETA * error * ((*itrinp).x2)); //calcula novo valor de w2

			pesosPrev = pesosAT;
			std::cout << "PREV | W0: " << pesosPrev.w0 << " | W1: "<< pesosPrev.w1 << " | W2: " << pesosPrev.w2 << std::endl;
		}
		epochs++;
		std::cout << "Erro esperado: " << ERRESP << " | Erro Atual: " << error << " | Epoca: " << epochs <<std::endl;
	}
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "W0: " << pesosAT.w0 << " | W1: "<< pesosAT.w1 << " | W2: " << pesosAT.w2 << std::endl;

	for(itrinp = entradasValid.begin(); itrinp != entradasValid.end();itrinp++)
	{
		v = (pesosAT.w0 * (*itrinp).bias) + (pesosAT.w1 * (*itrinp).x1) + ((*itrinp).x2 * pesosAT.w2);
		y = funcAtiv(v); //passa o resultado da soma das multiplicacoes das entradas pelos pesos respectivos pela
		std::cout << "Y: " << y << std::endl;
		//funcao de ativacao

		error = (*itrinp).esperado - y; //calcula o erro para o valor esperado desse cenario
		std::cout << "Error: " << error << std::endl;

	}


	return 0;

}
