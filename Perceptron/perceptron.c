#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>


#define ETA 0.001 //1.e-3
#define ERROESP 0.0001//1.e-4

double rand01()
{
	return (double)(rand())/(double)(RAND_MAX);
}
int main(int argc, char *argv[])
{

	printf("Este programa classifica duas entradas em duas classe, 1 e 2, de acordo com o grafico apresentado abaixo.\n");
	srand(time(NULL));
	double v; //saida do somatorio da multiplicacao da entradas pelos pesos
	double y; //saida, valor de V apos passar pela funcao de ativacao
	double error;
	double pesos[3][1];
	double pesosprev[3][1];
	int desejadoTrein[11] = {1,1,1,1,2,2,2,2,2,2,2};
	//int desejadoValid[10] = ;
	int dcount;//contador do tamanho dos valores desejados
	int i,j,k;
	for(i=0; i < 3; i++)
	{
		usleep(100000);
		pesos[i][1] = rand01();
		pesosprev[i][1] = pesos[i][1];
		printf("Peso W%d: %f\n",i,pesos[i][1]);
	}

	double entradasTreina[11][3] = {
		{-1,0.45,0},
		{-1,0.5,0.3},
		{-1,0.2,0.4},
		{-1,0.6,0.5},
		{-1,0.6,0},
		{-1,0.8,0.1},
		{-1,1.4,0},
		{-1,0.7,0.2},
		{-1,1.2,0.2},
		{-1,1.3,0.2},
		{-1,0.8,0.4}};//3 colunas pois uma sera preenchida com o -1 do bias
	double entradasValida[10][3] = {
		{-1,0.4,0.7},
		{-1,0.8,0.9},
		{-1,0.9,1},
		{-1,1,0.4},
		{-1,1.2,0.4},
		{-1,0.8,0.5},
		{-1,1,0.5},
		{-1,1.1,0.6},
		{-1,1,0.7},
		{-1,1,0.8}};
	//while(error > ERROESP)
	//{
		for(i=0;i<11;i++)
		{
			for(j = 0;j<3;j++)
			{
				v += entradasTreina[i][j] * pesos[j][i];
				printf("V: %f\n",v);
			}
			y = 1/(1+exp((v)*(-1)));// funcao logistica
			printf("Y: %f\n",y);
			error = desejadoTrein[i] - y;
			printf("Error: %f\n",error);
			for(k = 0;k < 3;k++)
			{
				pesos[k][1] = pesosprev[k][1] + (ETA*error*entradasTreina[i][k]);
				pesosprev[k][1] = pesos[k][1];
			}
		}
		for(i = 0;i< (sizeof(pesos)/sizeof(double));i++)
		{
			printf("Peso W%d: %f \n",i,pesos[i][1]);

		}
		printf("Erro final: %f \n",error);
	//}
	return 0;
}




