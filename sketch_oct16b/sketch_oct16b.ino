#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <avr/io.h>
#include <stdio.h>



#define NROWS 4

int main()
{
    Serial.begin(115200);
    int aux1[4][4];
    int i,j;
      for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            aux1[i][j] = -1;
        }
    }
    for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            aux1[i][j] = rand()%4;
        }
    }
    Serial.println();
    Serial.print("######NORMAL##########\n");
    for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            
            Serial.print(aux1[i][j]);
            Serial.print("\t");
        }
        Serial.print("\n");
    }
    int *m = aux1[0];
    Serial.print("######PTR##########\n");
    for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {

            Serial.print(m[j+i*NROWS]);
            Serial.print("\t");
        }
        Serial.print("\n");
    }
        for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {

            Serial.print(m[j+i*NROWS]);
            Serial.print("\t");
        }
        Serial.print("\n");
    }
    Serial.print("*m[3][0] = ");
    Serial.print(m[12]);
    Serial.print("\t");
    Serial.print("*m[3][1] = ");
    Serial.print(m[13]);
    Serial.print("\t");
    Serial.print("*m[3][2] = ");
    Serial.print(m[14]);
    Serial.print("\t");
    Serial.print("*m[3][3] = ");
    Serial.println(m[15]);
return 0;
}


